var JalaliDate = function () {
    var _year = 0, _month = 0, _day = 0, _hours = 0, _minutes = 0, _seconds = 0;
    //------------------------------------------------------------------------------------------------------------------
    var setYear = function (year) {
        _year = year;
    };
    var getYear = function () {
        return _year;
    };
    var setMonth = function (month) {
        _month = month;
    };
    var getMonth = function () {
        return _month;
    };
    var setDay = function (day) {
        _day = day;
    };
    var getDay = function () {
        return _day;
    };
    var setHours = function (hours) {
        _hours = hours;
    };
    var getHours = function () {
        return _hours;
    };
    var setMinutes = function (minutes) {
        _minutes = minutes;
    };
    var getMinutes = function () {
        return _minutes;
    };
    var setSeconds = function (seconds) {
        _seconds = seconds;
    };
    var getSeconds = function () {
        return _seconds;
    };

    var getMonthName = function () {
        return JalaliDate.GetMonthName(getMonth());
    };
    var getDayOfWeek = function () {
        var stamp = getTimestamp();
        var timestamp = new Date(stamp * 1000);
        return (timestamp.getDay() + 1) % 7;
    };
    var getDayName = function (short) {
        return JalaliDate.GetDayName(getDayOfWeek(), short);
    };
    var getTimestamp = function () {
        return JalaliDate.ToTimestamp(getYear(), getMonth(), getDay(), getHours(), getMinutes(), getSeconds());
    };
    var toString = function () {
        return getYear() + '/' + getMonth() + '/' + getDay() + ' ' + getHours() + ':' + getMinutes();
    };
    //------------------------------------------------------------------------------------------------------------------
    this.setYear = setYear;
    this.getYear = getYear;
    this.setMonth = setMonth;
    this.getMonth = getMonth;
    this.setDay = setDay;
    this.getDay = getDay;
    this.setHours = setHours;
    this.getHours = getHours;
    this.setMinutes = setMinutes;
    this.getMinutes = getMinutes;
    this.setSeconds = setSeconds;
    this.getSeconds = getSeconds;

    this.getMonthName = getMonthName;
    this.getDayOfWeek = getDayOfWeek;
    this.getDayName = getDayName;
    this.getTimestamp = getTimestamp;
    this.toString = toString;
};

JalaliDate.MillisecondsToSeconds = function (milliseconds) {
    if (milliseconds < 1000) return milliseconds;
    return Math.ceil(milliseconds / 1000);
};

JalaliDate.ToTimestamp = function (year, month, day, hours, minutes, seconds) {
    if (year === undefined || year === 'now') {
        return JalaliDate.MillisecondsToSeconds(Date.now());
    }

    if (year instanceof JalaliDate) {
        hours = year.getHours();
        minutes = year.getMinutes();
        seconds = year.getSeconds();
        day = year.getDay();
        month = year.getMonth();
        year = year.getYear();
    }

    var gregorianDate = JalaliDate.JalaliToGregorian(year, month, day);
    if(hours != undefined && minutes != undefined && seconds != undefined) {
        gregorianDate.setHours(hours);
        gregorianDate.setMinutes(minutes);
        gregorianDate.setSeconds(seconds);
        gregorianDate.setMilliseconds(0);
    }

    return JalaliDate.MillisecondsToSeconds(gregorianDate.getTime());
};

JalaliDate.ToDate = function (timestamp) {
    if (timestamp === undefined) {
        timestamp = JalaliDate.ToTimestamp('now');
    }
    var temp = new Date(timestamp * 1000);
    return JalaliDate.GregorianToJalali(temp);
};

JalaliDate.StartDayOfMonth = function (year, month) {
    if (year instanceof JalaliDate) {
        month = year.getMonth();
        year = year.getYear();
    }
    var stamp = JalaliDate.ToTimestamp(year, month, 1, 0, 0, 0);
    var timestamp = new Date(stamp * 1000);
    return (timestamp.getDay() + 1) % 7;
};

JalaliDate.LastDayOfMonth = function (year, month) {
    if (year instanceof JalaliDate) {
        month = year.getMonth();
        year = year.getYear();
    }
    var day = JalaliDate.GetDaysOfMonth(year, month);
    var stamp = JalaliDate.ToTimestamp(year, month, day, 0, 0, 0);
    var timestamp = new Date(stamp * 1000);
    return (timestamp.getDay() + 1) % 7;
};

JalaliDate.GetDaysOfMonth = function (year, month) {
    if (year instanceof JalaliDate) {
        month = year.getMonth();
        year = year.getYear();
    }
    var countDays = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29];
    if (month < 1 || month > 12) {
        throw month + " Out Of Range Exception";
    }

    if (JalaliDate.IsLeap(year) && month == 12) return 30;

    return countDays[month - 1];
};

JalaliDate.GetDaysOfGregorianMonth = function (month) {
    var countDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (month < 1 || month > 12) {
        throw month + " Out Of Range Exception";
    }
    return countDays[month - 1];
};

JalaliDate.GetMonthName = function (month) {
    var months = ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"];
    return months[parseInt(month - 1)];
};

JalaliDate.GetGregorianMonthName = function (month) {
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return months[parseInt(month - 1)];
};

JalaliDate.GetDayName = function (dayOfWeek, shortName) {
    var days = ["شنبه", "یک شنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنج شنبه", "جمعه"];
    if (shortName)
        return days[parseInt(dayOfWeek)].substr(0, 1);

    return days[parseInt(dayOfWeek)];
};

JalaliDate.GetGregorianDayName = function (dayOfWeek, shortName) {
    var days = ["Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
    if (shortName)
        return days[parseInt(dayOfWeek)].substr(0, 2);

    return days[parseInt(dayOfWeek)];
};

JalaliDate.ConvertNumberToPersian = function (date) {
    var temp = date || "";
    var PersianNumbers = ['\u06F0', '\u06F1', '\u06F2', '\u06F3', '\u06F4', '\u06F5', '\u06F6', '\u06F7', '\u06F8', '\u06F9'];
    var part1 = "";

    for (var i = 0; i < date.length; i++) {
        if (parseInt(date[i]) >= 0 && parseInt(date[i]) <= 9) {
            part1 = temp.substring(0, i);
            part1 = part1.concat(PersianNumbers[date[i]]);
            temp = part1.concat(temp.substring(i + 1, temp.length));
        }
    }

    return temp;
};

JalaliDate.ConvertNumberToEnglish = function (date) {
    var temp = date || "";
    var PersianNumbers = ['\u06F0', '\u06F1', '\u06F2', '\u06F3', '\u06F4', '\u06F5', '\u06F6', '\u06F7', '\u06F8', '\u06F9'];
    var part1 = "";

    for (var i = 0; i < date.length; i++) {
        if (date[i] >= PersianNumbers[0] && date[i] <= PersianNumbers[9]) {
            part1 = temp.substring(0, i);
            part1 = part1.concat(String(PersianNumbers.indexOf(date[i])));
            temp = part1.concat(temp.substring(i + 1, temp.length));
        }
    }

    return temp;
};

JalaliDate.GregorianToJalali = function (gregorianYear, gregorianMonth, gregorianDay) {
    var gregorianHours = 0, gregorianMinutes = 0, gregorianSeconds = 0;

    if (gregorianYear instanceof Date) {
        gregorianHours = gregorianYear.getHours();
        gregorianMinutes = gregorianYear.getMinutes();
        gregorianSeconds = gregorianYear.getSeconds();
        gregorianDay = gregorianYear.getDate();
        gregorianMonth = gregorianYear.getMonth() + 1;
        gregorianYear = gregorianYear.getFullYear();
    }

    gregorianYear -= 1600;
    gregorianMonth -= 1;
    gregorianDay -= 1;

    var gregorianDayNo = (gregorianYear * 365) + parseInt((gregorianYear + 3) / 4) - parseInt((gregorianYear + 99) / 100) + parseInt((gregorianYear + 399) / 400);

    var i;

    for (i = 1; i <= gregorianMonth; i++)
        gregorianDayNo += JalaliDate.GetDaysOfGregorianMonth(i);

    if (gregorianMonth > 1 && (((gregorianYear % 4 == 0) && (gregorianYear % 100 != 0)) || (gregorianYear % 400 == 0)))
        gregorianDayNo++; //Leap and after Feb

    gregorianDayNo += gregorianDay;
    var jalaliDayNo = gregorianDayNo - 79;
    var jalaliNP = parseInt(jalaliDayNo / 12053); // 12053 = 365 * 33 + 32 / 4
    jalaliDayNo %= 12053;
    var jalaliYear = 979 + 33 * jalaliNP + 4 * parseInt(jalaliDayNo / 1461); // 1461 = 365 * 4 + 4 / 4
    jalaliDayNo %= 1461;

    if (jalaliDayNo >= 366) {
        jalaliYear += parseInt((jalaliDayNo - 1) / 365);
        jalaliDayNo = (jalaliDayNo - 1) % 365;
    }

    for (i = 1; i < 12 && jalaliDayNo >= JalaliDate.GetDaysOfMonth(jalaliYear, i); i++)
        jalaliDayNo -= JalaliDate.GetDaysOfMonth(jalaliYear, i);

    var jalaliMonth = i;
    var jalaliDay = jalaliDayNo + 1;

    var jalaliDate = new JalaliDate();
    jalaliDate.setYear(jalaliYear);
    jalaliDate.setMonth(jalaliMonth);
    jalaliDate.setDay(jalaliDay);
    jalaliDate.setHours(gregorianHours);
    jalaliDate.setMinutes(gregorianMinutes);
    jalaliDate.setSeconds(gregorianSeconds);

    return jalaliDate;
};

JalaliDate.JalaliToGregorian = function (jalaliYear, jalaliMonth, jalaliDay) {
    var time = new Date();
    var jalaliHours = time.getHours(), jalaliMinutes = time.getMinutes(), jalaliSeconds = time.getSeconds();
    var jYear = jalaliYear instanceof JalaliDate ? jalaliYear.getYear() : jalaliYear;
    if (jalaliYear instanceof JalaliDate) {
        jalaliHours = jalaliYear.getHours();
        jalaliMinutes = jalaliYear.getMinutes();
        jalaliSeconds = jalaliYear.getSeconds();
        jalaliDay = jalaliYear.getDay();
        jalaliMonth = jalaliYear.getMonth();
        jalaliYear = jalaliYear.getYear();
    }

    jalaliYear -= 979;
    jalaliMonth -= 1;
    jalaliDay -= 1;

    var jalaliDayNo = 365 * jalaliYear + parseInt(jalaliYear / 33) * 8 + parseInt((jalaliYear % 33 + 3) / 4);
    var i;

    for (i = 1; i <= jalaliMonth; ++i) {
        jalaliDayNo += JalaliDate.GetDaysOfMonth(jYear, i);
    }

    jalaliDayNo += jalaliDay;
    var gregorianDayNo = jalaliDayNo + 79;
    var gregorianYear = 1600 + 400 * parseInt(gregorianDayNo / 146097); //146097 = 365*400 + 400/4 - 400/100 + 400/400
    gregorianDayNo %= 146097;
    var leap = true;

    if (gregorianDayNo >= 36525) { //36525 = 365*100 + 100/4
        gregorianDayNo--;
        gregorianYear += 100 * parseInt(gregorianDayNo / 36524); //36524 = 365*100 + 100/4 - 100/100
        gregorianDayNo %= 36524;

        if (gregorianDayNo >= 365)
            gregorianDayNo++;
        else
            leap = false;
    }

    gregorianYear += 4 * parseInt(gregorianDayNo / 1461); //1461 = 365*4 + 4/4
    gregorianDayNo %= 1461;

    if (gregorianDayNo >= 366) {
        leap = false;
        gregorianDayNo--;
        gregorianYear += parseInt(gregorianDayNo / 365);
        gregorianDayNo %= 365;
    }

    for (i = 1; gregorianDayNo >= JalaliDate.GetDaysOfGregorianMonth(i) + (i == 2 && leap); i++) {
        gregorianDayNo -= JalaliDate.GetDaysOfGregorianMonth(i) + (i == 2 && leap);
    }

    var gregorianMonth = i - 1; // because in Date, month start with 0
    var gregorianDay = gregorianDayNo + 1;

    var gregorianDate = new Date();
    gregorianDate.setFullYear(gregorianYear);
    gregorianDate.setMonth(gregorianMonth);
    gregorianDate.setDate(gregorianDay);
    gregorianDate.setHours(jalaliHours);
    gregorianDate.setMinutes(jalaliMinutes);
    gregorianDate.setSeconds(jalaliSeconds);
    return gregorianDate;
};

JalaliDate.IsLeap = function (year) {
    if (year instanceof JalaliDate) {
        year = year.getYear();
    }
    var leapYearsFirst = [1, 5, 9, 13, 17, 22, 26, 30];
    var leapYearsSecond = [1, 5, 9, 13, 18, 22, 26, 30];
    var mod = 0;
    if (1244 <= year && year <= 1342) {
        mod = year % 33;
        for (var i in leapYearsFirst) {
            if (leapYearsFirst[i] == mod)
                return true;
        }
    }
    else if (1243 <= year && year <= 1472) {
        mod = year % 33;
        for (var i in leapYearsSecond) {
            if (leapYearsSecond[i] == mod)
                return true;
        }
    }
    return false;
};