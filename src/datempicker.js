var DateMPicker = function (inputElement) {

    var today = JalaliDate.ToDate();
    var currentYear = today.getYear();
    var currentMonth = today.getMonth();
    var selectedTime = today.getTimestamp();

    if(inputElement.getAttribute('data-timestamp')) {
        var date = JalaliDate.ToDate(inputElement.getAttribute('data-timestamp'));
        selectedTime = date.getTimestamp();
        currentYear = date.getYear();
        currentMonth = date.getMonth();
    }

    var changeSelectedDay;
    var currentDayElement;

    var mpicker = document.createElement('div');
    mpicker.className = "mpicker mpicker--opened mpicker--focused";
    var mpickerHolder = document.createElement('div');
    mpickerHolder.className = "mpicker__holder";
    var mpickerFrame = document.createElement('div');
    mpickerFrame.className = "mpicker__frame";
    var mpickerWrap = document.createElement('div');
    mpickerWrap.className = "mpicker__wrap";

    var changeDate = function () {
        var box = mpickerWrap.querySelector('div.mpicker__box');
        var display = box.children[0];
        var time = box.children[2];
        var footer = box.children[3];

        box.innerHTML = "";

        box.appendChild(display);
        box.appendChild(createCalendarContainer());
        box.appendChild(time);
        box.appendChild(footer);
    };
    var createMPickerBox = function () {
        var container = document.createElement('div');
        container.className = "mpicker__box";

        container.appendChild(createDateDisplay());
        container.appendChild(createCalendarContainer());
        container.appendChild(createTimeMPicker());
        container.appendChild(createFooter());

        return container;
    };
    var createDateDisplay = function () {
        var container = document.createElement('div');
        container.className = "mpicker__date-display";

        var weekdayDisplay = document.createElement('div');
        var monthDisplay = document.createElement('div');
        var dayDisplay = document.createElement('div');
        var yearDisplay = document.createElement('div');

        var selectedDay = JalaliDate.ToDate(selectedTime);

        weekdayDisplay.innerHTML = selectedDay.getDayName();
        weekdayDisplay.className = "mpicker__weekday-display";
        monthDisplay.innerHTML = '<div>' + selectedDay.getMonthName() + '</div>';
        monthDisplay.className = "mpicker__month-display";
        dayDisplay.innerHTML = '<div>' + selectedDay.getDay() + '</div>';
        dayDisplay.className = "mpicker__day-display";
        yearDisplay.innerHTML = '<div>' + selectedDay.getYear() + '</div>';
        yearDisplay.className = "mpicker__year-display";

        container.appendChild(weekdayDisplay);
        container.appendChild(monthDisplay);
        container.appendChild(dayDisplay);
        container.appendChild(yearDisplay);

        changeSelectedDay = function () {
            var selectedDay = JalaliDate.ToDate(selectedTime);
            weekdayDisplay.innerHTML = selectedDay.getDayName();
            monthDisplay.innerHTML = '<div>' + selectedDay.getMonthName() + '</div>';
            dayDisplay.innerHTML = '<div>' + selectedDay.getDay() + '</div>';
            yearDisplay.innerHTML = '<div>' + selectedDay.getYear() + '</div>';
        };

        return container;
    };
    var createCalendarContainer = function () {
        var container = document.createElement('div');
        container.className = "mpicker__calendar-container";

        container.appendChild(createCalendarHeader());
        container.appendChild(createCalendarTable());

        return container;
    };
    var createCalendarTable = function () {
        var dayTime = 24 * 60 * 60;
        var startCurrentMonth = JalaliDate.StartDayOfMonth(currentYear, currentMonth);
        var FirstDayOfMonth = JalaliDate.ToTimestamp(currentYear, currentMonth, 1);
        var shiftLeftDayTime = startCurrentMonth * dayTime;
        var startTime = FirstDayOfMonth - shiftLeftDayTime;
        var currentTime = startTime;

        var table = document.createElement('table');
        table.className = "mpicker__table";

        var tHead = document.createElement('thead');
        var tHeadRow = document.createElement('tr');
        for (var i = 0; i < 7; i++) {
            var th = '<th class="mpicker__weekday" title="' + JalaliDate.GetDayName(i) + '" scope="col">' + JalaliDate.GetDayName(i, true) + '</th>';
            tHeadRow.innerHTML += th;
        }

        var tBody = document.createElement('tbody');

        var currentDay;
        for (var ri = 0; ri < 6; ri++) {
            if (currentDay && currentDay.getMonth() > currentMonth) break;
            var tr = document.createElement('tr');
            for (var ti = 0; ti < 7; ti++) {
                currentDay = JalaliDate.ToDate(currentTime);
                var td = '<td role="presentation"><div class="mpicker__day mpicker__day--infocus" role="gridcell" data-pick="' + currentDay.getTimestamp() + '" aria-label="' + currentDay.toString() + '">' + currentDay.getDay() + '</div></td>';
                if (currentTime === today.getTimestamp()) {
                    td = '<td role="presentation"><div class="mpicker__day mpicker__day--infocus mpicker__day--today mpicker__day--highlighted" role="gridcell" data-pick="' + currentDay.getTimestamp() + '" aria-label="' + currentDay.toString() + '">' + currentDay.getDay() + '</div></td>';
                }
                if (currentDay.getMonth() !== currentMonth) {
                    td = '<td role="presentation"><div class="mpicker__day mpicker__day--outfocus" role="gridcell" data-pick="' + currentDay.getTimestamp() + '" aria-label="' + currentDay.toString() + '">' + currentDay.getDay() + '</div></td>';
                }
                tr.innerHTML += td;
                currentTime += dayTime;
            }
            tBody.appendChild(tr);
        }

        tHead.appendChild(tHeadRow);
        table.appendChild(tHead);
        table.appendChild(tBody);

        currentDayElement = function (element) {
            if (element) {
                table.currentDay = element;
            }
            else {
                return table.currentDay;
            }
        };

        var cells = table.querySelectorAll('div.mpicker__day--infocus');
        for (var i in cells) {
            cells[i].onclick = function (event) {
                selectedTime = event.target.getAttribute('data-pick');
                changeSelectedDay();

                if (currentDayElement()) {
                    currentDayElement().className = 'mpicker__day mpicker__day--infocus';
                    if (currentDayElement().getAttribute('data-pick') == today.getTimestamp()) {
                        currentDayElement().className = 'mpicker__day mpicker__day--infocus mpicker__day--today mpicker__day--highlighted';
                    }
                }
                event.target.className = 'mpicker__day mpicker__day--infocus mpicker__day--selected mpicker__day--highlighted';
                currentDayElement(event.target);
            };
        }

        return table;
    };
    var createCalendarHeader = function () {

        var selectedDay = JalaliDate.ToDate(JalaliDate.ToTimestamp(currentYear, currentMonth, 1));

        var container = document.createElement('div');
        container.className = "mpicker__header";

        var divMonth = document.createElement('div');
        var selectYear = document.createElement('select');
        var navPrev = document.createElement('div');
        var navNext = document.createElement('div');

        divMonth.className = 'mpicker__month';
        divMonth.innerHTML = selectedDay.getMonthName();
        selectYear.className = 'mpicker__select--year browser-default';
        selectYear.title = "انتخاب سال";
        selectYear.setAttribute('aria-controls', '');
        for (var i = selectedDay.getYear() - 15; i < selectedDay.getYear() + 15; i++) {
            if (i === selectedDay.getYear())
                selectYear.innerHTML += '<option selected="" value="' + i + '">' + i + '</option>';
            selectYear.innerHTML += '<option value="' + i + '">' + i + '</option>';
        }
        selectYear.addEventListener('change', function () {
            currentYear = selectYear.value;
            changeDate();
        });
        navPrev.className = 'mpicker__nav--prev';
        navPrev.setAttribute('data-nav', '1');
        navPrev.setAttribute('role', 'button');
        navPrev.setAttribute('title', 'ماه قبل');
        navPrev.setAttribute('aria-controls', '');
        navPrev.onclick = function () {
            currentMonth--;
            if (currentMonth === 0) {
                currentYear--;
                currentMonth = 12;
            }
            changeDate();
        };
        navNext.className = 'mpicker__nav--next';
        navNext.setAttribute('data-nav', '-1');
        navNext.setAttribute('role', 'button');
        navNext.setAttribute('title', 'ماه بعد');
        navNext.setAttribute('aria-controls', '');
        navNext.onclick = function () {
            currentMonth++;
            if (currentMonth === 13) {
                currentYear++;
                currentMonth = 1;
            }
            changeDate();
        };

        container.appendChild(divMonth);
        container.appendChild(selectYear);
        container.appendChild(navPrev);
        container.appendChild(navNext);

        return container;
    };
    var createTimeMPicker = function () {
        var container = document.createElement('div');
        container.className = "mpicker__time";

        var inputHours = document.createElement('input');
        inputHours.className = "mpicker__time-hours";
        inputHours.setAttribute('type', 'number');
        inputHours.setAttribute('placeholder', 'ساعت');
        inputHours.style.textAlign = 'center';
        inputHours.min = 0;
        inputHours.max = 23;
        inputHours.value = today.getHours();
        inputHours.onchange = function() {
            if(inputHours.value > 23) inputHours.value = 23;
            if(inputHours.value < 0) inputHours.value = 0;
            var tempDate = new Date(selectedTime * 1000);
            tempDate.setHours(inputHours.value);
            selectedTime = JalaliDate.MillisecondsToSeconds(tempDate.getTime());
        };

        var inputMinutes = document.createElement('input');
        inputMinutes.className = "mpicker__time-minutes";
        inputMinutes.setAttribute('type', 'number');
        inputMinutes.setAttribute('placeholder', 'دقیقه');
        inputMinutes.style.textAlign = 'center';
        inputMinutes.value = today.getMinutes();
        inputMinutes.min = 0;
        inputMinutes.max = 59;
        inputMinutes.onchange = function() {
            if(inputMinutes.value > 59) inputMinutes.value = 59;
            if(inputMinutes.value < 0) inputMinutes.value = 0;
            var tempDate = new Date(selectedTime * 1000);
            tempDate.setMinutes(inputMinutes.value);
            selectedTime = JalaliDate.MillisecondsToSeconds(tempDate.getTime());
        };

        var separator = document.createElement('div');
        separator.className = "mpicker__time-separator";
        separator.style.textAlign = "center";
        separator.innerHTML = ":";

        container.appendChild(inputHours);
        container.appendChild(separator);
        container.appendChild(inputMinutes);

        return container;
    };
    var createFooter = function () {
        var container = document.createElement('div');
        container.className = "mpicker__footer";

        var buttonToday = document.createElement('button');
        var buttonClear = document.createElement('button');
        var buttonClose = document.createElement('button');

        buttonToday.className = "btn-flat mpicker__today";
        buttonToday.setAttribute('type', 'button');
        buttonToday.setAttribute('data-pick', today.getTimestamp());
        buttonToday.setAttribute('aria-controls', '');
        buttonToday.innerHTML = 'امروز';
        buttonToday.onclick = function () {
            selectedTime = today.getTimestamp();
            currentMonth = today.getMonth();
            currentYear = today.getYear();

            changeSelectedDay();
            changeDate();
        };
        buttonClear.className = "btn-flat mpicker__clear";
        buttonClear.setAttribute('type', 'button');
        buttonClear.setAttribute('data-clear', '1');
        buttonClear.setAttribute('aria-controls', '');
        buttonClear.innerHTML = 'پاک کن';
        buttonClear.onclick = function () {
            selectedTime = 0;
            inputElement.value = "";
            inputElement.setAttribute('data-timestamp', '');
            mpicker.remove();
        };
        buttonClose.className = "btn-flat mpicker__close";
        buttonClose.setAttribute('type', 'button');
        buttonClose.setAttribute('data-close', 'true');
        buttonClose.setAttribute('aria-controls', '');
        buttonClose.innerHTML = 'بستن';
        buttonClose.onclick = function () {
            inputElement.value = JalaliDate.ToDate(selectedTime).toString();
            inputElement.setAttribute('data-timestamp', selectedTime);
            mpicker.remove();
        };

        container.appendChild(buttonToday);
        container.appendChild(buttonClear);
        container.appendChild(buttonClose);

        return container;
    };

    mpickerWrap.appendChild(createMPickerBox());
    mpickerFrame.appendChild(mpickerWrap);
    mpickerHolder.appendChild(mpickerFrame);
    mpicker.appendChild(mpickerHolder);
    document.body.appendChild(mpicker);
};
